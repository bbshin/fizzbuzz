import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;

/**
 *
 */

/**
 * @author matsunaga.shinya
 *
 */
public class FizzBuzzTest {
	/**
	 * {@link FizzBuzz#fizzbuzz(int)} のためのテスト・メソッド。
	 */
	@Test
	public void testFizzbuzz1() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));
		FizzBuzz fb = new FizzBuzz();
		fb.fizzbuzz(2);
		assertThat(out.toString(), is(2 + System.lineSeparator()));
	}

	@Test
	public void testFizzbuzz2() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));
		FizzBuzz fb = new FizzBuzz();
		fb.fizzbuzz(3);
		assertThat(out.toString(), is("Fizz" + System.lineSeparator()));
	}

	@Test
	public void testFizzbuzz3() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));
		FizzBuzz fb = new FizzBuzz();
		fb.fizzbuzz(4);
		assertThat(out.toString(), is(4 + System.lineSeparator()));
	}

	@Test
	public void testFizzbuzz4() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));
		FizzBuzz fb = new FizzBuzz();
		fb.fizzbuzz(5);
		assertThat(out.toString(), is("Buzz" + System.lineSeparator()));
	}

	@Test
	public void testFizzbuzz5() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));
		FizzBuzz fb = new FizzBuzz();
		fb.fizzbuzz(6);
		assertThat(out.toString(), is("Fizz" + System.lineSeparator()));
	}

	@Test
	public void testFizzbuzz6() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));
		FizzBuzz fb = new FizzBuzz();
		fb.fizzbuzz(14);
		assertThat(out.toString(), is(14 + System.lineSeparator()));
	}

	@Test
	public void testFizzbuzz7() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));
		FizzBuzz fb = new FizzBuzz();
		fb.fizzbuzz(15);
		assertThat(out.toString(), is("FizzBuzz" + System.lineSeparator()));
	}

	@Test
	public void testFizzbuzz8() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));
		FizzBuzz fb = new FizzBuzz();
		fb.fizzbuzz(16);
		assertThat(out.toString(), is(16 + System.lineSeparator()));
	}

}
